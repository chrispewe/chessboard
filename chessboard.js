const makeChessboard = () => {
  let chessboard = []

  // ... write your code here
  
  //coba while
  let i = 0
  while (i <= 7){ 
    let rowtemp = []
    const chessplayer = ["B", "K", "M", "RT", "RJ", "M", "K", "B"];
    //coba for
    for (let j = 0; j <= 7; j++){
      //coba if condition
      if(i == 0){
        //coba array push
        rowtemp.push(chessplayer[j] + ' Black');
      } else if (i == 1 || i==6){
        //coba conditional (ternary) operator 
        i==1 ? rowtemp.push("P Black") : rowtemp.push("P White") 
      } else if (i == 7){
        rowtemp.push(chessplayer[j] + ' White');
      } else{
        rowtemp.push('-')
      }
    }
    chessboard.push(rowtemp);
    i++ 
  }
  return chessboard
}

const printBoard = x => {
  // ... write your code here
  console.log(x)
}

printBoard(makeChessboard())